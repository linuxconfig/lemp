FROM linuxconfig/nginx
MAINTAINER Lubos Rendek <web@linuxconfig.org>

ENV DEBIAN_FRONTEND noninteractive

# Main package installation
RUN apt-get update
RUN apt-get -y install supervisor php-cgi mariadb-server php-mysql 

# Extra package installation
RUN apt-get -y install php-gd php-apcu php-cli php-fpm php-curl php-pear php-dev libmcrypt-dev
# Install mcrypt
RUN pecl install mcrypt
RUN echo "extension=mcrypt.so" >> /etc/php/7.4/cgi/php.ini
RUN echo "extension=mcrypt.so" >> /etc/php/7.4/cli/php.ini

# Nginx configuration
ADD default /etc/nginx/sites-available/

# PHP FastCGI script
ADD php-fcgi /usr/local/sbin/
RUN chmod o+x /usr/local/sbin/php-fcgi

# Supervisor configuration files
ADD supervisord.conf /etc/supervisor/
ADD supervisor-lemp.conf /etc/supervisor/conf.d/

# Basic PHP website
ADD index.php /var/www/html/


# Create new MySQL admin user
RUN service mariadb start; mysql -u root -e "CREATE USER 'admin'@'%' IDENTIFIED BY 'pass';";mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%' WITH GRANT OPTION;";

# MySQL configuration
RUN sed -i 's/bind-address/#bind-address/' /etc/mysql/my.cnf

# Clean up
RUN apt-get clean

EXPOSE 80 3306

CMD ["supervisord"]
